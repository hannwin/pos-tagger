The script Perceptron.py contains a perceptron based part of speech tagger
Following steps are implemented: 
 1) extract features for a given word in given pos-tagged file
 2) build a nested dictionary with each tag and its occurring features and weighths (weights are feature counts)
 3) have the model guess the POS tag for requested word, given the previously counted weights
 4) tune the weights for an incorrectly guessed POS tag (increase correct and decrease incorrect feature weights)
 5) repeat 3) and 4) until correct tag is predicted by model

To run the code following arguments are required:
1) training file
2) test file
3) feature combinations
example: python3 Perceptron.py train.col dev.col 123





The script Evaluation.py calculates micro and macro Precision, Recall and F-Score.
To run the script, following arguments are required:
1) flie with gold standard: dev_stripped.txt
2) file with predicted tags: prediction.txt
The script requires the files to be of same length, therfore hte file dev_stripped.txt was created (no newlines).
Evaluation.py writes its predictions in a file called prediction.txt. 
The scores will be printed into the shell.