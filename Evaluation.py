
class Evaluation():

    def __init__(self, doc_gold, doc_pred):
        self.doc_pred = doc_pred
        self.doc_gold = doc_gold


    def corpusCheck(self):
        """checks if files (with correct tags and predicted tags) are of the same length """
        lengthCheck = False
        countGold = 0
        countPred = 0
        #opens both files and counts non-empty lines
        with open (self.doc_gold, "rt") as gold, open (self.doc_pred, "rt") as pred:
            for line in gold:
                if line.split():
                    countGold += 1
            for line in pred:
                if line.split():
                    countPred += 1
            #if files are of same length, method returns True
            if countGold == countPred:
                lengthCheck = True
        return lengthCheck



    def corpus(self):
        """returns a list of lists that contain each pair: text - tag_gold - tag_predicted """
        corpus_list = []
        count_lists = 0
        
        #writes text plus tag of gold standard into a list (creates list of lists)
        with open(self.doc_gold, "rt") as gold:
            for line in gold:
                if line.split():
                    corpus_list.append(line.split())


        #appends predicted tag to previous list            
        with open(self.doc_pred, "rt") as pred:
            for line in pred:
                if line.split():
                    corpus_list[count_lists].append(line.split()[1])
                    count_lists += 1

        return corpus_list
            
            

    def count_tags(self, corpus_list):
        """Takes counts of fp, tp, fn for all tags and writes them into a dictionary """
        dic_counts = {}

        #creates a dictionary entry for each tag with the values being another dictionary that counts tp, fp, fn
        for tagList in corpus_list:
        	#tagList[1] corresponds to the gold standard's tag,
            if tagList[1] not in dic_counts:
                dic_counts[tagList[1]] = {"tp": 0, "fp": 0, "fn": 0}  
            #tagList[2] corresponds to the predicted tag
            if tagList[2] not in dic_counts:
                dic_counts[tagList[2]] = {"tp": 0, "fp": 0, "fn": 0}
                
            #adds counts to tp, fp, fn for each tag
            if tagList[1] == tagList[2]:
                dic_counts[tagList[1]]["tp"] += 1   
            else: 
                dic_counts[tagList[1]]["fn"] += 1
                dic_counts[tagList[2]]["fp"] += 1
    
        return dic_counts
          

        
    def microAverage(self, dic_counts):
        """Calculates micro average"""
        sum_tp = 0   #sum of all tp of all tags
        sum_fp = 0   #sum of all fp of all tags
        sum_fn = 0   #sum of all fn of all tags

        #counts tp, fp, fn of all tags
        for tag in dic_counts:
            sum_tp += dic_counts[tag]["tp"]
            sum_fp += dic_counts[tag]["fp"]
            sum_fn += dic_counts[tag]["fn"]

        #calculates and prints all micro scores
        microPrecision = sum_tp / (sum_tp + sum_fp)
        microRecall = sum_tp / (sum_tp + sum_fn)
        microFScore = 2 * microRecall * microPrecision / (microRecall + microPrecision)
        print("Micro Precision: " + str(round(microPrecision, 3)))
        print("Micro Recall: " + str(round(microRecall, 3)))
        print("Micro F-Score: " + str(round(microFScore, 3)))

        return microFScore



    def macroAverage(self, dic_counts):
        """Calculates macro average"""
        sum_Prec = 0 #variable for Precision
        sum_Rec = 0 #variable for Recall

        for tag in dic_counts:
            try:
            	#calculates precision for each tag
                precision_tag = dic_counts[tag]["tp"] / (dic_counts[tag]["tp"] + dic_counts[tag]["fp"])
                sum_Prec += precision_tag
                #calculates recall for each tag
                recall_tag = dic_counts[tag]["tp"] / (dic_counts[tag]["tp"] + dic_counts[tag]["fn"])
                sum_Rec += recall_tag
            #in case the counts for either tp+fp (precision) or tp+fn (recall) are zero
            except ZeroDivisionError:
                pass

        #calculates and prints all macro scores
        macroPrec = sum_Prec / len(dic_counts)
        macroRec = sum_Rec / len(dic_counts)
        macroFScore = 2 * macroRec * macroPrec / (macroPrec + macroRec)
        print("Macro Precision: " + str(round(macroPrec, 3)))
        print("Macro Recall: " + str(round(macroRec, 3)))
        print("Macro F-Score: " + str(round(macroFScore, 3)))

        return macroFScore


    def main(self):
    	#if Eval is imported as module, only this function needs to be called with the according documents as arguments
    	#checks if documents are of same length
        tags = self.corpus()
        counted_tags = self.count_tags(tags)

        if self.corpusCheck():
            self.microAverage(counted_tags)
            self.macroAverage(counted_tags)
        else:
            print("The documents are of different length.")



if __name__ == '__main__':
	#calling code in shell
    import sys
    #gold standard is first argument
    doc_gold = sys.argv[1]
    #file with predictions is second argument
    doc_pred = sys.argv[2]
    
    execute = Evaluation(doc_gold, doc_pred)
    execute.main()

   
